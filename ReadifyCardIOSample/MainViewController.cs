﻿using System;
using MonoTouch.Dialog;
using ReadifyCardIO;
using UIKit;

namespace ReadifyCardIOSample
{
    public class MainViewController : DialogViewController, ICardIOPaymentViewControllerDelegate
	{
		const string _title = "Readify Card IO";

		public MainViewController () : base(UITableViewStyle.Plain, new RootElement(_title), false)
		{
		}

        CardIOPaymentViewController paymentViewController;

		StyledStringElement elemCardNumber;

		public override void ViewDidLoad ()
		{
			base.ViewDidLoad ();
            	
			elemCardNumber = new StyledStringElement ("xxxx xxxx xxxx xxxx");

			Root = new RootElement (_title) {
				new Section {
					elemCardNumber,
					new StyledStringElement("Enter your Credit Card", () => {
                        paymentViewController = new CardIOPaymentViewController (this);
						paymentViewController.HideCardIOLogo = true;

						NavigationController.PresentViewController(paymentViewController, true, null);
					}) { Accessory = UITableViewCellAccessory.DisclosureIndicator }
				}
			};
		}

        public void UserDidCancelPaymentViewController (CardIOPaymentViewController paymentViewController)
        {
        	paymentViewController.DismissViewController(true, null);
        }
        public void UserDidProvideCreditCardInfo (CardIOCreditCardInfo cardInfo, CardIOPaymentViewController paymentViewController)
        {
            if (cardInfo == null) {
                elemCardNumber.Caption = "xxxx xxxx xxxx xxxx";
                Console.WriteLine("Cancelled");
            } else {
                elemCardNumber.Caption = cardInfo.CardNumber;
            }

            ReloadData();

            paymentViewController.DismissViewController(true, null);        
        }
	}
}