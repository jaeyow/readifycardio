﻿using System;

using UIKit;
using Foundation;
using ObjCRuntime;
using CoreGraphics;

namespace ReadifyCardIO
{
	// @interface CardIOCreditCardInfo : NSObject <NSCopying>
	[BaseType(typeof(NSObject))]
	interface CardIOCreditCardInfo : INSCopying
	{
		// @property (readwrite, copy, nonatomic) NSString * cardNumber;
		[Export("cardNumber")]
		string CardNumber { get; set; }

		// @property (readonly, copy, nonatomic) NSString * redactedCardNumber;
		[Export("redactedCardNumber")]
		string RedactedCardNumber { get; }

		// @property (assign, readwrite, nonatomic) NSUInteger expiryMonth;
		[Export("expiryMonth")]
		nuint ExpiryMonth { get; set; }

		// @property (assign, readwrite, nonatomic) NSUInteger expiryYear;
		[Export("expiryYear")]
		nuint ExpiryYear { get; set; }

		// @property (readwrite, copy, nonatomic) NSString * cvv;
		[Export("cvv")]
		string Cvv { get; set; }

		// @property (readwrite, copy, nonatomic) NSString * postalCode;
		[Export("postalCode")]
		string PostalCode { get; set; }

		// @property (readwrite, copy, nonatomic) NSString * cardholderName;
		[Export("cardholderName")]
		string CardholderName { get; set; }

		// @property (assign, readwrite, nonatomic) BOOL scanned;
		[Export("scanned")]
		bool Scanned { get; set; }

		// @property (readwrite, nonatomic, strong) UIImage * cardImage;
		[Export("cardImage", ArgumentSemantic.Strong)]
		UIImage CardImage { get; set; }

		// @property (readonly, assign, nonatomic) CardIOCreditCardType cardType;
		[Export("cardType", ArgumentSemantic.Assign)]
		CardIOCreditCardType CardType { get; }

		// +(NSString *)displayStringForCardType:(CardIOCreditCardType)cardType usingLanguageOrLocale:(NSString *)languageOrLocale;
		[Static]
		[Export("displayStringForCardType:usingLanguageOrLocale:")]
		string DisplayStringForCardType(CardIOCreditCardType cardType, string languageOrLocale);

		// +(UIImage *)logoForCardType:(CardIOCreditCardType)cardType;
		[Static]
		[Export("logoForCardType:")]
		UIImage LogoForCardType(CardIOCreditCardType cardType);
	}

	// @protocol CardIOViewDelegate <NSObject>
	[Protocol, Model]
	[BaseType(typeof(NSObject))]
	interface CardIOViewDelegate
	{
		// @required -(void)cardIOView:(CardIOView *)cardIOView didScanCard:(CardIOCreditCardInfo *)cardInfo;
		[Abstract]
		[Export("cardIOView:didScanCard:")]
		void DidScanCard(CardIOView cardIOView, CardIOCreditCardInfo cardInfo);
	}

	// @interface CardIOView : UIView
	[BaseType(typeof(UIView))]
	interface CardIOView
	{
		[Wrap("WeakDelegate")]
		CardIOViewDelegate Delegate { get; set; }

		// @property (readwrite, nonatomic, weak) id<CardIOViewDelegate> delegate;
		[NullAllowed, Export("delegate", ArgumentSemantic.Weak)]
		NSObject WeakDelegate { get; set; }

		// @property (readwrite, copy, nonatomic) NSString * languageOrLocale;
		[Export("languageOrLocale")]
		string LanguageOrLocale { get; set; }

		// @property (readwrite, retain, nonatomic) UIColor * guideColor;
		[Export("guideColor", ArgumentSemantic.Retain)]
		UIColor GuideColor { get; set; }

		// @property (assign, readwrite, nonatomic) BOOL useCardIOLogo;
		[Export("useCardIOLogo")]
		bool UseCardIOLogo { get; set; }

		// @property (assign, readwrite, nonatomic) BOOL hideCardIOLogo;
		[Export("hideCardIOLogo")]
		bool HideCardIOLogo { get; set; }

		// @property (assign, readwrite, nonatomic) BOOL allowFreelyRotatingCardGuide;
		[Export("allowFreelyRotatingCardGuide")]
		bool AllowFreelyRotatingCardGuide { get; set; }

		// @property (readwrite, copy, nonatomic) NSString * scanInstructions;
		[Export("scanInstructions")]
		string ScanInstructions { get; set; }

		// @property (readwrite, retain, nonatomic) UIView * scanOverlayView;
		[Export("scanOverlayView", ArgumentSemantic.Retain)]
		UIView ScanOverlayView { get; set; }

		// @property (assign, readwrite, nonatomic) BOOL scanExpiry;
		[Export("scanExpiry")]
		bool ScanExpiry { get; set; }

		// @property (assign, readwrite, nonatomic) CardIODetectionMode detectionMode;
		[Export("detectionMode", ArgumentSemantic.Assign)]
		CardIODetectionMode DetectionMode { get; set; }

		// @property (assign, readwrite, nonatomic) CGFloat scannedImageDuration;
		[Export("scannedImageDuration")]
		nfloat ScannedImageDuration { get; set; }

		// @property (readonly, assign, nonatomic) CGRect cameraPreviewFrame;
		[Export("cameraPreviewFrame", ArgumentSemantic.Assign)]
		CGRect CameraPreviewFrame { get; }
	}

	[Static]
	//Verify(ConstantsInterfaceAssociation)]
	partial interface Constants
	{
		// extern NSString *const CardIOScanningOrientationDidChangeNotification;
		[Field("CardIOScanningOrientationDidChangeNotification", "__Internal")]
		NSString CardIOScanningOrientationDidChangeNotification { get; }

		// extern NSString *const CardIOCurrentScanningOrientation;
		[Field("CardIOCurrentScanningOrientation", "__Internal")]
		NSString CardIOCurrentScanningOrientation { get; }

		// extern NSString *const CardIOScanningOrientationAnimationDuration;
		[Field("CardIOScanningOrientationAnimationDuration", "__Internal")]
		NSString CardIOScanningOrientationAnimationDuration { get; }
	}

	// @protocol CardIOPaymentViewControllerDelegate <NSObject>
	[Protocol, Model]
	[BaseType(typeof(NSObject))]
	interface CardIOPaymentViewControllerDelegate
	{
		// @required -(void)userDidCancelPaymentViewController:(CardIOPaymentViewController *)paymentViewController;
		[Abstract]
		[Export("userDidCancelPaymentViewController:")]
		void UserDidCancelPaymentViewController(CardIOPaymentViewController paymentViewController);

		// @required -(void)userDidProvideCreditCardInfo:(CardIOCreditCardInfo *)cardInfo inPaymentViewController:(CardIOPaymentViewController *)paymentViewController;
		[Abstract]
		[Export("userDidProvideCreditCardInfo:inPaymentViewController:")]
		void UserDidProvideCreditCardInfo(CardIOCreditCardInfo cardInfo, CardIOPaymentViewController paymentViewController);
	}

	interface ICardIOPaymentViewControllerDelegate { }

	// @interface CardIOPaymentViewController : UINavigationController
	[BaseType(typeof(UINavigationController))]
	interface CardIOPaymentViewController
	{
		// -(id)initWithPaymentDelegate:(id<CardIOPaymentViewControllerDelegate>)aDelegate;
		[Export("initWithPaymentDelegate:")]
		IntPtr Constructor(ICardIOPaymentViewControllerDelegate aDelegate);

		// -(id)initWithPaymentDelegate:(id<CardIOPaymentViewControllerDelegate>)aDelegate scanningEnabled:(BOOL)scanningEnabled;
		[Export("initWithPaymentDelegate:scanningEnabled:")]
		IntPtr Constructor(ICardIOPaymentViewControllerDelegate aDelegate, bool scanningEnabled);

		// @property (readwrite, copy, nonatomic) NSString * languageOrLocale;
		[Export("languageOrLocale")]
		string LanguageOrLocale { get; set; }

		// @property (assign, readwrite, nonatomic) BOOL keepStatusBarStyle;
		[Export("keepStatusBarStyle")]
		bool KeepStatusBarStyle { get; set; }

		// @property (assign, readwrite, nonatomic) UIBarStyle navigationBarStyle;
		[Export("navigationBarStyle", ArgumentSemantic.Assign)]
		UIBarStyle NavigationBarStyle { get; set; }

		// @property (readwrite, retain, nonatomic) UIColor * navigationBarTintColor;
		[Export("navigationBarTintColor", ArgumentSemantic.Retain)]
		UIColor NavigationBarTintColor { get; set; }

		// @property (assign, readwrite, nonatomic) BOOL disableBlurWhenBackgrounding;
		[Export("disableBlurWhenBackgrounding")]
		bool DisableBlurWhenBackgrounding { get; set; }

		// @property (readwrite, retain, nonatomic) UIColor * guideColor;
		[Export("guideColor", ArgumentSemantic.Retain)]
		UIColor GuideColor { get; set; }

		// @property (assign, readwrite, nonatomic) BOOL suppressScanConfirmation;
		[Export("suppressScanConfirmation")]
		bool SuppressScanConfirmation { get; set; }

		// @property (assign, readwrite, nonatomic) BOOL suppressScannedCardImage;
		[Export("suppressScannedCardImage")]
		bool SuppressScannedCardImage { get; set; }

		// @property (assign, readwrite, nonatomic) CGFloat scannedImageDuration;
		[Export("scannedImageDuration")]
		nfloat ScannedImageDuration { get; set; }

		// @property (assign, readwrite, nonatomic) BOOL maskManualEntryDigits;
		[Export("maskManualEntryDigits")]
		bool MaskManualEntryDigits { get; set; }

		// @property (readwrite, copy, nonatomic) NSString * scanInstructions;
		[Export("scanInstructions")]
		string ScanInstructions { get; set; }

		// @property (assign, readwrite, nonatomic) BOOL hideCardIOLogo;
		[Export("hideCardIOLogo")]
		bool HideCardIOLogo { get; set; }

		// @property (readwrite, retain, nonatomic) UIView * scanOverlayView;
		[Export("scanOverlayView", ArgumentSemantic.Retain)]
		UIView ScanOverlayView { get; set; }

		// @property (assign, readwrite, nonatomic) CardIODetectionMode detectionMode;
		[Export("detectionMode", ArgumentSemantic.Assign)]
		CardIODetectionMode DetectionMode { get; set; }

		// @property (assign, readwrite, nonatomic) BOOL collectExpiry;
		[Export("collectExpiry")]
		bool CollectExpiry { get; set; }

		// @property (assign, readwrite, nonatomic) BOOL collectCVV;
		[Export("collectCVV")]
		bool CollectCVV { get; set; }

		// @property (assign, readwrite, nonatomic) BOOL collectPostalCode;
		[Export("collectPostalCode")]
		bool CollectPostalCode { get; set; }

		// @property (assign, readwrite, nonatomic) BOOL restrictPostalCodeToNumericOnly;
		[Export("restrictPostalCodeToNumericOnly")]
		bool RestrictPostalCodeToNumericOnly { get; set; }

		// @property (assign, readwrite, nonatomic) BOOL collectCardholderName;
		[Export("collectCardholderName")]
		bool CollectCardholderName { get; set; }

		// @property (assign, readwrite, nonatomic) BOOL scanExpiry;
		[Export("scanExpiry")]
		bool ScanExpiry { get; set; }

		// @property (assign, readwrite, nonatomic) BOOL useCardIOLogo;
		[Export("useCardIOLogo")]
		bool UseCardIOLogo { get; set; }

		// @property (assign, readwrite, nonatomic) BOOL allowFreelyRotatingCardGuide;
		[Export("allowFreelyRotatingCardGuide")]
		bool AllowFreelyRotatingCardGuide { get; set; }

		// @property (assign, readwrite, nonatomic) BOOL disableManualEntryButtons;
		[Export("disableManualEntryButtons")]
		bool DisableManualEntryButtons { get; set; }

		[Wrap("WeakPaymentDelegate")]
		CardIOPaymentViewControllerDelegate PaymentDelegate { get; set; }

		// @property (readwrite, nonatomic, weak) id<CardIOPaymentViewControllerDelegate> paymentDelegate;
		[NullAllowed, Export("paymentDelegate", ArgumentSemantic.Weak)]
		NSObject WeakPaymentDelegate { get; set; }
	}

	// @interface NonConflictingAPINames (CardIOPaymentViewController)
	[Category]
	[BaseType(typeof(CardIOPaymentViewController))]
	interface CardIOPaymentViewController_NonConflictingAPINames
	{
		// @property (nonatomic, assign, readwrite) BOOL keepStatusBarStyleForCardIO;
		[Export("keepStatusBarStyleForCardIO")]
		bool GetKeepStatusBarStyleForCardIO();

		[Export("setKeepStatusBarStyleForCardIO:")]
		void SetKeepStatusBarStyleForCardIO(bool keep);

		// @property (nonatomic, assign, readwrite) UIBarStyle navigationBarStyleForCardIO;
		[Export("navigationBarStyleForCardIO")]
		UIBarStyle GetNavigationBarStyleForCardIO();

		[Export("setNavigationBarStyleForCardIO:")]
		void SetNavigationBarStyleForCardIO(UIBarStyle navigationBarStyle);

		// @property (nonatomic, retain, readwrite) UIColor* navigationBarTintColorForCardIO;
		[Export("navigationBarTintColorForCardIO", ArgumentSemantic.Retain)]
		UIColor NavigationBarTintColorForCardIO();

		[Export("setNavigationBarTintColorForCardIO:")]
		void SetNavigationBarTintColorForCardIO(UIColor tintColor);
	}

	// @interface CardIOUtilities : NSObject
	[BaseType(typeof(NSObject))]
	interface CardIOUtilities
	{
		// +(NSString *)libraryVersion;
		[Static]
		[Export("libraryVersion")]
		//[Verify(MethodToProperty)]
		string LibraryVersion { get; }

		// +(BOOL)canReadCardWithCamera;
		[Static]
		[Export("canReadCardWithCamera")]
		//[Verify(MethodToProperty)]
		bool CanReadCardWithCamera { get; }

		// +(void)preload;
		[Static]
		[Export("preload")]
		void Preload();

		// +(UIImageView *)blurredScreenImageView;
		[Static]
		[Export("blurredScreenImageView")]
		//[Verify(MethodToProperty)]
		UIImageView BlurredScreenImageView { get; }
	}

	// @interface NonConflictingAPINames (CardIOUtilities)
	[Category]
	[BaseType(typeof(CardIOUtilities))]
	interface CardIOUtilities_NonConflictingAPINames
	{
		// +(NSString *)cardIOLibraryVersion;
		[Static]
		[Export("cardIOLibraryVersion")]
		string CardIOLibraryVersion { get; }

		// +(void)preloadCardIO;
		[Static]
		[Export("preloadCardIO")]
		void PreloadCardIO();
	}
}
