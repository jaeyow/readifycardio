﻿using System;
using ObjCRuntime;

namespace ReadifyCardIO
{
	[Native]
	public enum CardIOCreditCardType : long
	{
		Unrecognized = 0,
		Ambiguous = 1,
		Amex = 51,
		Jcb = 74,
		Visa = 52,
		Mastercard = 53,
		Discover = 54
	}

	[Native]
	public enum CardIODetectionMode : long
	{
		CardImageAndNumber = 0,
		CardImageOnly,
		Automatic
	}
}
