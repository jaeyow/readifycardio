﻿using ObjCRuntime;

[assembly: LinkWith("libCardIO.a", 
    IsCxx = true, 
    LinkTarget = LinkTarget.ArmV7 | LinkTarget.ArmV7s | LinkTarget.Simulator, 
    ForceLoad = true, 
    Frameworks = "Accelerate AudioToolbox AVFoundation CoreGraphics CoreMedia CoreVideo Foundation MobileCoreServices OpenGLES QuartzCore Security UIKit", 
    LinkerFlags = "-ObjC -lc++")]

